**INF0333 - Aplicação de uma ferramenta para organização de um projeto ágil**

**Professores**

- Thelma Cecília S. Chiossi
- José Alexandre D'Abruzzo Pereira



**Integrantes - Equipe 6**
- Daniel Salgado Costa - 5963397-2
- Flavia Machado Vilar - 54285915-4
- Gerson Macedo - 27081121-7
- Mayara Ferreira Fernandes - 55462125-3
- Tulio Bassaco Bustos - 44659139-7



**Objetivo**
Este é a trabalho da matéria INF0333, que tem como objetivo elaborar um Release Plan para o desenvolvimento completo de um projeto e configurá-lo na ferramenta de gestão de projeto ágil de software.
